import React, { Component } from 'react';
import Mensajes from '../mensajes/mensajes.js'
import Home from '../home/home.js'
import { BrowserRouter as Router,
  Switch,
  Route,
  Redirect,
  useRouteMatch,
  useParams } from 'react-router-dom';

export default function RouterComponent() {
    return (
        <Router>
          <Route path="/home">
            <Home />
          </Route>
          <Switch>
            <Route exact path="/">
              <Home />
            </Route> 
            <Route path="/mensajes">
              <Mensajes />
            </Route>            
          </Switch>
        </Router>
    );
}  