import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Switch from '@material-ui/core/Switch';



export default function SpeedDials() {
  
  const [hidden, setHidden] = React.useState(false);

  const handleHiddenChange = event => {
    setHidden(event.target.checked);
  };

  return (
    <FormControlLabel
    control={
      <Switch checked={hidden} onChange={handleHiddenChange} value="hidden" color="primary" />
    }
    label=""
  />
  )
}