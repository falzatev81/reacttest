import React from "react";
import Popup from "reactjs-popup";
import Modal from 'react-awesome-modal';
 
export default () => (
  <Modal 
      visible={this.state.visible}
      width="400"
      height="300"
      effect="fadeInUp"
      onClickAway={() => this.closeModal()}
  >
      <div>
          <h1>Title</h1>
          <p>Some Contents</p>
          <a href="javascript:void(0);" onClick={() => this.closeModal()}>Close</a>
      </div>
  </Modal>
);