import React, { Component } from 'react'
import './filtros.css';
import SpeedDials from '../funcionc';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Switch from '@material-ui/core/Switch';
import { faLightbulb, faShoppingBag, faHeart, faCalendarAlt, faSearch} from '@fortawesome/free-solid-svg-icons'

class Filtros extends Component {
    

    render() {
        return (
            <div className="contenedorfiltros">
                <div>
                    <header className="headerfiltros">
                        <FontAwesomeIcon className="LightbulbIcon" size="1x" icon={faLightbulb} /> 
                        <label>Smart Filter</label>
                    </header>
                    <hr className="sepHorFiltros" />

                    <div className="seccionfiltros">
                        <div className="iconfiltro">
                            <FontAwesomeIcon className="ShoppingBagIcon" size="2x" icon={faShoppingBag} />
                        </div>
                        <div className="trogglefiltro">
                        <SpeedDials />
                        </div>
                    </div>

                    <div className="seccionfiltros">
                        <div className="iconfiltro">
                            <FontAwesomeIcon className="ShoppingBagIcon" size="2x" icon={faCalendarAlt} />
                        </div>
                        <div className="trogglefiltro">
                        <SpeedDials />
                        </div>
                    </div>

                    <div className="seccionfiltros">
                        <div className="iconfiltro">
                            <FontAwesomeIcon className="ShoppingBagIcon" size="2x" icon={faHeart} />
                        </div>
                        <div className="trogglefiltro">
                        <SpeedDials />
                        </div>
                    </div>

                    <hr className="sepHorFiltros" />

                    <div id="footerfiltros">
                        <label>Status</label>
                        <input type="text" id="inputsearchfiltro" placeholder="search" />
                        <input type="text" id="inputdeliver" placeholder="DELIVER" />
                    </div>
                </div>
                <hr className="sepVerFiltros" />
            </div>
        )
    }
}

export default Filtros;