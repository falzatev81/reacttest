import React, { useState, Component } from 'react';
import Checkbox from '@material-ui/core/Checkbox';
import CircleChecked from '@material-ui/icons/CheckCircleOutline';
import CircleCheckedFilled from '@material-ui/icons/CheckCircle';
import CircleUnchecked from '@material-ui/icons/RadioButtonUnchecked';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faTruck, faChevronDown, faLocationArrow, 
    faEllipsisV, faPlusCircle } from '@fortawesome/free-solid-svg-icons'
import { tsConstructorType } from '@babel/types';

export default class EntregasFunction extends Component {

    constructor(props) {
        super(props);       

        this.state = {
            
            Entregas: [
                {Id: 1, Fecha: "Monday 10", Hora: "3:28 PM", Desde: "Houston, TX, 33619", Hacia: "Atlanta, CA, 30123", Valor: 250000.00, cantidad: 2, checked: false},
                {Id: 2, Fecha: "Tuesday 25", Hora: "2:28 PM", Desde: "New York, NY 44619", Hacia: "Miami, FL 30123", Valor: 950000.00, cantidad: 3, checked: false},
                {Id: 3, Fecha: "Monday 15", Hora: "6:28 AM", Desde: "Houston, TX, 33619", Hacia: "Boston, MA 30123", Valor: 150000.00, cantidad: 1, checked: false}
              ]
        }
    }

    ChangestateQ = () => {        
        this.setState({isChecked: !this.state.isChecked})        
    }
    
    cambiar(i, estado){
        this.props.changeChecked();
        const _newEntregas = this.state.Entregas;
        _newEntregas[i].checked =!estado; 
        alert(i + " " + estado + " - " +_newEntregas[i].checked )
        this.setState({Entregas: _newEntregas})
    }
    //const [check, setcheck] = useState(props.isChecked); 

    // const numbers = props.numbers;
    
    render() {
        if(this.props.isChecked){
            this.state.Entregas.map(entrega => {
                entrega.checked = true;                
            })
        }else{
            this.state.Entregas.map(entrega => {
                entrega.checked = false;
            })
        }
        const listItem = this.state.Entregas.map((entrega, i) =>
      <div className="continfo" key={i}>
            <Checkbox key={i}
                icon={<CircleUnchecked />}
                checkedIcon={entrega.checked != true ?<CircleChecked /> : <CircleCheckedFilled />} 
                checked={entrega.checked} 
                onChange={() => this.cambiar(i, entrega.checked)} 
            />
            <div className="infoentrega" key={i}>
                <div className="fecha" key={i}>
                <label htmlFor="" className="descfecha" key={i}>{entrega.Fecha}<sup>th</sup></label>
                    <label htmlFor="" className="descfecha" key={i}>{entrega.Hora}</label>
                </div>
                <hr className="separadorvertical" />                      
                <div className="ubicacion" key={i}>
                    <div className="paperarrow" key={i}>
                        <FontAwesomeIcon className="arrowIcon" size="2x" icon={faLocationArrow} key={i} />
                    </div>
                    <div className="ublabel">
                        <label htmlFor="" className="">{entrega.Desde}</label>
                        <label htmlFor="" ><FontAwesomeIcon className="cevronIcon" size="2x" icon={faChevronDown} /></label>
                        <label>{entrega.Hacia}</label>
                    </div>
                </div>
                <hr className="separadorvertical" />
                <div className="precioEntrega">
                    <FontAwesomeIcon className="truckIcon" size="4x" icon={faTruck} />
                    <label className="valor">{entrega.Valor}</label>
                    <div className="cantidad">{entrega.cantidad}</div>
                    <FontAwesomeIcon className="ellinpsisIcon" size="3x" icon={faEllipsisV} />
                </div>
            </div>
        </div>
    );
        return (
            <div>
                 {listItem} 
            </div>
          );
    }
  }
