import React, { Component } from 'react'
import logo from '../logo.PNG';
import './menuprincipal.css'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faHome, faMailBulk, faStar, faCog, faUserAlt, react } from '@fortawesome/free-solid-svg-icons'


class MenuPrincipal extends Component {
    render() {
        return (
            <header className="App-header">
                <div className="logo"><img src={logo} className="App-logo" alt="logo" /></div>
                <nav>
                    <ul>
                        <li><a href="/" className="nav-link"><FontAwesomeIcon className="fontA" size="2x" icon={faHome} />Home</a></li>
                        <li><a href="mensajes" className="nav-link"><FontAwesomeIcon className="fontA" size="2x" icon={faMailBulk} />Messages</a></li>
                        <li><a href="#" className="nav-link"><FontAwesomeIcon className="fontA" size="2x" icon={faStar} />Wishist</a></li>
                        <li><a href="#" className="nav-link"><FontAwesomeIcon className="fontA" size="2x" icon={faCog} />Settings</a></li>
                        <li><a href="#" className="nav-link"><FontAwesomeIcon className="fontA" size="2x" icon={faUserAlt} />My Account</a></li>
                    </ul>
                </nav>
                
            </header>
        )
    }
}

export default MenuPrincipal;