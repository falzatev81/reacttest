import React, { Component } from 'react';
import MenuPrincipal from '../menuprincipal/menuprincipal.js'
import Contenido from '../contenido/contenido.js'
import Filtros from '../filtros/filtros.js'

class Home extends Component {
    render() {
        return (
            <div>                
                <Filtros className="filtros"/>
                <Contenido className="contenido" />
            </div>
        )
    }
}

export default Home;