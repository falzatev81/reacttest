import React from 'react';
import './App.css'
import MenuPrincipal from  './menuprincipal/menuprincipal.js'
import Contenido from './contenido/contenido.js'
import Filtros from './filtros/filtros.js'
import RouterComponent from './routercomponent/routercomponent.js'

function App() {
  return (
    <div className="App">
        <MenuPrincipal />
        <RouterComponent />        
    </div>
  );
}

export default App;
