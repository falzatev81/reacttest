import React, { Component } from 'react'
import './contenido.css' 
import Checkbox from '@material-ui/core/Checkbox';
import CircleChecked from '@material-ui/icons/CheckCircleOutline';
import CircleCheckedFilled from '@material-ui/icons/CheckCircle';
import CircleUnchecked from '@material-ui/icons/RadioButtonUnchecked';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faTruck, faChevronDown, faLocationArrow, 
    faEllipsisV, faPlusCircle } from '@fortawesome/free-solid-svg-icons'

import Modal from 'react-awesome-modal';
import EntregasFunction from '../entregasfunction'
    
class Contenido extends Component {

    constructor(props) {
        super(props)
        
        this.state = {
            isChecked: false,
            visible : false
        }
    }
    
    Changestate = () => {        
        this.setState({isChecked: !this.state.isChecked})        
    }

    openModal() {
        this.setState({
            visible : true
        });
    }

    closeModal() {
        this.setState({
            visible : false
        });
    }

    changeChecked= (e) =>{
        this.setState({
            isChecked : false
        });
    }

    render() {
        return (
            <main id="contenedormain">
                <Modal 
                    visible={this.state.visible}
                    width="400"
                    height="300"
                    effect="fadeInUp"
                    onClickAway={() => this.closeModal()}
                >
                    <div id="modalentrega">
                        <h1>Title</h1>
                        <p>Some Contents</p>
                        <a href="javascript:void(0);" onClick={(e) => this.closeModal(e)}>Close</a>
                    </div>
                </Modal>
                <section id="search" >
                    <Checkbox 
                        icon={<CircleUnchecked size="3x" />}
                        checkedIcon={this.state.isChecked != true ? <CircleChecked /> : <CircleCheckedFilled />}
                        checked={ this.state.isChecked } 
                        onChange={() => this.Changestate()}
                    />
                    <input type="text" id="inputsearch" placeholder="search" />
                    
                </section>
                <section id="entrega">
                    <EntregasFunction isChecked={this.state.isChecked} changeChecked={this.changeChecked} />
                    <FontAwesomeIcon className="pluscircle" size="4x" icon={faPlusCircle} onClick={() => this.openModal()}/>
                                                      
                </section>             
            </main>
        )
    }
}

export default Contenido;